# Curriculum vitae - Luka Vidakovic

HTML: [Luka_Vidakovic-CV.html](https://vidakovicluka.gitlab.io/curriculum-vitae/Luka_Vidakovic-CV.html)

PDF: [Luka_Vidakovic-CV.pdf](https://vidakovicluka.gitlab.io/curriculum-vitae/Luka_Vidakovic-CV.pdf)

Markdown: [Luka_Vidakovic-CV.md](https://gitlab.com/vidakovicluka/curriculum-vitae/-/blob/main/Luka_Vidakovic-CV.md?ref_type=heads)
