.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Luka's Vidakovic Curriculum-Vitae
====================================================

.. toctree::
   :maxdepth: 3

   Luka_Vidakovic-CV