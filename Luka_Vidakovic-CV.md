---
colorlinks: true
---
HTML: [Luka_Vidakovic-CV.html](https://vidakovicluka.gitlab.io/curriculum-vitae/Luka_Vidakovic-CV.html)  
Markdown: [Luka_Vidakovic-CV.md](https://gitlab.com/vidakovicluka/curriculum-vitae/-/blob/main/Luka_Vidakovic-CV.md?ref_type=heads)  
PDF: [Luka_Vidakovic-CV.pdf](https://vidakovicluka.gitlab.io/curriculum-vitae/Luka_Vidakovic-CV.pdf)
---

address: Fruskogorska 1 (Science and Technology park Novi Sad), 21102, Novi Sad, Serbia  
email: luka.vidakovic@labsoft.dev  
phone: [+381641443512](tel:+381641443512)

---

![](resources/Luka_Vidakovic.png)

# Luka Vidakovic

**Innovative AI & Cloud Engineer | Co-Founder at StartAI & LabSoft AI | Generative AI & Cloud Computing Specialist | Passionate Educator**

[![LabSoft](resources/LabSoft.png)](https://labsoft.ai) [![StartAI](resources/StartAI.png)](https://startai.tech)

<p align="center">
  <a href="https://gitlab.com/vidakovicluka">
    <img src="https://img.shields.io/badge/Follow-%40vidakovicluka-green?logo=gitlab">
  </a>
  <a href="https://github.com/LukaVidakovic">
    <img src="https://img.shields.io/badge/Follow-%40LukaVidakovic-green?logo=github">
  </a> 
  <a href="https://www.linkedin.com/in/lukavidakovic/">
    <img src="https://img.shields.io/badge/Connect-%40lukavidakovic-blue?logo=linkedin">
  </a>
</p>

## About Me

As a passionate AI & Cloud Engineer and Entrepreneur, I thrive at the intersection of technology and innovation. With expertise in Generative AI, Cloud Computing, and Software Development, I'm dedicated to creating solutions that drive both business success and societal progress.

**Entrepreneurial Ventures:**
- **Co-Founder at StartAI**: Revolutionizing customer support through AI-powered chatbots, virtual assistants, and voice bots integrated with platforms like WhatsApp and Instagram. Built on AWS with Generative AI for data-driven decision making.
- **Co-Founder at LabSoft AI**: Leading the Generative AI revolution since 2023, developing applications that enhance business efficiency while serving as a catalyst for societal advancement.

**Mission:** To harness AI and cloud technologies for transformative solutions that address today's challenges and empower future innovation.

## Skills

**Core Competencies:**
- Generative AI (RAG, GPTs, Prompt Engineering)
- Cloud Computing (AWS, Serverless Architectures)
- Software Architecture & System Design
- DevOps & CI/CD Pipelines

**Technical Expertise:**
- **Languages**: Python, C/C++, Java, JavaScript, C#/.NET
- **AI/ML**: LLM Applications, Voice Recognition Systems
- **Cloud**: AWS Services, Scalable Serverless Solutions
- **Web Development**: React, Node.js, REST API Development

**Human Languages:**
- Serbian (Native)
- English (Fluent)

## Experience

### `2024-` Co-Founder & AI Software Engineer | **StartAI**
- Developed AI-powered customer support solutions integrating WhatsApp/Instagram
- Implemented voice-to-voice bots using generative AI models
- Designed AWS-based infrastructure for scalable chatbot deployments
- Reduced client response times by 60% through AI automation

### `2023-` Co-Founder & Software Developer | **LabSoft AI**
- Led development of Hospy - AI-powered medical reporting platform
- Implemented multi-model AI architecture for voice/text processing
- Designed secure AWS serverless infrastructure
- Presented solutions at Data Science Conference Europe 2024

### `2023-2024` Lecturer in Programming | **Gymnasium Jovan Jovanović Zmaj**
- Developed adaptive curriculum blending theory with hands-on practice
- Taught Python/Java/C# fundamentals to 50+ students
- Created real-world projects improving exam scores by 30%

## Education

**B.Elec.Comp.Eng. (Expected 2025)**  
University of Novi Sad - Faculty of Technical Sciences  
Specialization: Computer Science
Relevant Courses: Distributed Systems, AI Fundamentals, Cloud Architectures

**Professional Certifications:**
- ITAcademy Java Development Program
- DeepLearning.AI Generative AI Specialization
- Intermediate English Course

